/*
 *  Name: Token.sol 
 *  Source: https://ethernaut.openzeppelin.com/level/0x6545df87f57d21cb096a0bfcc53a70464d062512
 *  Design flaws Indicators of TD: Arithmetic Issues
 *  Desgin Vulnerability: Integer Overflow - Underflow 
 *  CWE-682 
 *  SWC-101
 */ 


pragma solidity ^0.7.1;

contract Token {

  mapping(address => uint) balances;
  uint public totalSupply;

  constructor (uint _initialSupply) public {
    balances[msg.sender] = totalSupply = _initialSupply;
  }

  function transfer(address _to, uint _value) public returns (bool) {
    require(balances[msg.sender] - _value >= 0);
    balances[msg.sender] -= _value;
    balances[_to] += _value;
    return true;
  }

  function balanceOf(address _owner) public view returns (uint balance) {
    return balances[_owner];
  }
}
