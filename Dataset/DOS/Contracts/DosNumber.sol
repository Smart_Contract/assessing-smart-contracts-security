/*
 *  Name: DosAuction.sol 
 *  Source: https://swcregistry.io/docs/SWC-128
 *  Design flaws Indicators of TD: DOS
 *  Desgin Vulnerability: Costly pattern/Costly loop
 *  CWE-400 
 *  SWC-128 
 */

pragma solidity ^0.7.1;

contract DosNumber {

   uint numElements = 0;
    uint[] array;

    function insertNnumbers(uint value,uint numbers) public {

        // Gas DOS if number > 382 more or less, it depends on actual gas limit
        for(uint i=0;i<numbers;i++) {
            if(numElements == array.length) {
                //array.length += 1;
		array.push(); 
            }
            array[numElements++] = value;S
        }
    }

    function clear() public {
        require(numElements>1500);
        numElements = 0;
    }

    // Gas DOS clear
    function clearDOS() public {

        // number depends on actual gas limit
        require(numElements>1500);
        array = new uint[](0);
        numElements = 0;
    }

    function getLengthArray() public view returns(uint) {
        return numElements;
    }

    function getRealLengthArray() public view returns(uint) {
        return array.length;
    }
}
