/*
 *  Name: FunctionTypes.sol 
 *  Source: https://gist.github.com/wadeAlexC/7a18de852693b3f890560ab6a211a2b8
 *  Design flaws Indicators of TD: Access Control Broken
 *  Desgin Vulnerability: inline-assembly
 *  CWE-695
 *  SWC-127 
 *  Desgin Vulnerability: Unprotected Ether Withdrawal
 *  CWE-24
 *  SWC-105
 */ 

pragma solidity ^0.5.16;

contract FunctionTypes {
    
    constructor() public payable { require(msg.value != 0); }
    
    function withdraw() private {
        require(msg.value == 0, 'dont send funds!');
        address(msg.sender).transfer(address(this).balance);
    }
    
    function frwd() internal
        { withdraw(); }
        
    struct Func { function () internal f; }
    
    function breakIt() public payable {
        require(msg.value != 0, 'send funds!');
        Func memory func;
        func.f = frwd;
        assembly { mstore(func, add(mload(func), callvalue)) }
        func.f();
    }
}
