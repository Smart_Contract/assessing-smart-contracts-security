/*
 *  Name: lottopollo.sol 
 *  Source: https://github.com/seresistvanandras/EthBench/blob/master/Benchmark/Simple/timestampdependent.sol
 *  Design flaws Indicators of TD: Bad Randomness
 *  Desgin Vulnerability: Block timestamp dependency 
 *  CWE-820 
 *  SWC-116 
 */ 
 
pragma solidity ^0.7.1;

contract lottopollo {
  address payable leader;
  uint    timestamp;
  function payOut(uint rand) internal {
    // <yes> <report> TIME MANIPULATION
    if ( rand> 0 && block.timestamp - rand > 24 hours ) {
      msg.sender.transfer( msg.value );

      if ( address(this).balance > 0 ) {
        leader.transfer( address(this).balance );
      }
    }
    else if ( msg.value >= 1 ether ) {
      leader = msg.sender;
      timestamp = rand;
    }
  }
  function randomGen() public returns (uint randomNumber) {
      // <yes> <report> TIME MANIPULATION
      return block.timestamp;   
    }
  function draw(uint seed) public {
    uint randomNumber=randomGen(); 
    payOut(randomNumber);
  }
}
