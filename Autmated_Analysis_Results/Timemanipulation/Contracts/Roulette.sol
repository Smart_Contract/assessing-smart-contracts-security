/*
 * @source: https://github.com/sigp/solidity-security-blog
 *  Design flaws Indicators of TD: Time manipulation
 *  Desgin Vulnerability: Block timestamp dependency 
 *  CWE-820 
 *  SWC-116 
 */ 
 
 */

pragma solidity ^0.5.16;


contract Roulette {
    uint public pastBlockTime; // Forces one bet per block

   constructor() public payable {} // initially fund contract

    // fallback function used to make a bet
    fallback() external payable {
        require(msg.value == 10 ether); // must send 10 ether to play
        // <yes> <report> TIME_MANIPULATION
        require(block.timestamp != pastBlockTime); // only 1 transaction per block
        // <yes> <report> TIME_MANIPULATION
        pastBlockTime = block.timestamp;
        if(block.timestamp % 15 == 0) { // winner
            msg.sender.transfer(address(this).balance);
        }
    }
}
