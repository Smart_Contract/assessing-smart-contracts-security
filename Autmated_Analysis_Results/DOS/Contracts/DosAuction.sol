/*
 *  Name: DosAuction.sol 
 *  Source: https://github.com/crytic/not-so-smart-contracts/blob/master/denial_of_service/auction.sol
 *  Design flaws Indicators of TD: DOS
 *  Desgin Vulnerability: DoS by external contract/Call (Anti-Pattern) Pull over Push
 *  CWE-829 
 *  SWC-113 
 */

pragma solidity ^0.7.0;

//Auction susceptible to DoS attack
contract DosAuction {
  address payable currentFrontrunner;
  uint currentBid;

  //Takes in bid, refunding the frontrunner if they are outbid
  function bid() public payable {
    require(msg.value > currentBid);

    //If the refund fails, the entire transaction reverts.
    //Therefore a frontrunner who always fails will win
    if (currentFrontrunner != address(0)) {
      //E.g. if recipients fallback function is just revert()
      require(currentFrontrunner.send(currentBid));
    }

    currentFrontrunner = msg.sender;
    currentBid         = msg.value;
  }
}
