> ## Smart Contract Assessment Approach
> 
> 1.   Assessment_Approch Folder Includes: 
>		* Aggregated Vulnerabilities 
>       * 53 Security Analysis Tools
>       * Systematiclly Selection of 9 Automated Analysis Tools
>       * Mapping Design Vulnerabilities to CWEs
> 2.   Dataset Folder Includes: 
>       * 16 vulnerable smart contracts 
> 3.   Autmated_Analysis_Results Folder Includes: 
>       * Results of running 9 security analysis tools on the dataset 
> 4.   Experimental_Results Folder Includes: 
>      * Identification steps results in details
>      * Estimation steps results in details